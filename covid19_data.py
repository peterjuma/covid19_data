import requests

page=requests.get("https://www.worldometers.info/coronavirus/#countries")

import pandas as pd
from bs4 import BeautifulSoup

soup=BeautifulSoup(page.content, 'html.parser')
soup=BeautifulSoup(page.content, 'html.parser')
covid_data=soup.find(id='main_table_countries_today')

data = covid_data.find('tbody')

A=[]
B=[]
C=[]
D=[]
E=[]
F=[]
G=[]
H=[]
I=[]

for row in data.find_all("tr"):
    cells=row.find_all('td')
    if len(cells) ==9:
        A.append(cells[0].find(text=True))
        B. append(cells[1].find(text=True))
        C.append(cells[2].find(text=True))
        D.append(cells[3] .find(text=True))
        E.append(cells[4].find(text=True))
        F.append( cells[5].find(text=True))
        G.append(cells[6].find(text=True))
        H. append(cells[7].find(text=True))
        I.append(cells[7].find(text=True))

cases=pd.DataFrame (A, columns=['Country, other'])
cases['Total Cases']=B
cases['New Cases']=C
cases['Total Deaths']=D
cases['New Deaths']=E
cases['Active Cases']=F
cases['Total Recovered']=G
cases['serious/Critical']=H
cases['Tot Cases/1M pop']=I

cases.to_excel('covid19_data.xlsx')
