# Coronavirus Cases
***Confirmed Cases and Deaths by Country, Territory, or Conveyance***

The coronavirus COVID-19 is affecting 177 countries and territories around the world and 1 international conveyance (the Diamond Princess cruise ship harbored in Yokohama, Japan). The day is reset after midnight GMT+0. 
The "New" columns for China display the previous day changes (as China reports after the day is over). 
For all other countries, the "New" columns display the changes for the current day while still in progress.

*Data Source: https://www.worldometers.info/coronavirus/#countries*

